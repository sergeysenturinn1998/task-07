package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbUtils {

    public static Connection getConnection() throws DBException {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("app.properties"));
            String URL_CONNECTION = properties.getProperty("connection.url");

            return DriverManager.getConnection(URL_CONNECTION);
        } catch (SQLException | IOException e) {
            throw new DBException("no connection or file read fail", e);
        }
    }
    public static void close(AutoCloseable autoCloseable){
        if (autoCloseable!=null){
            try {
                autoCloseable.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public static void rollback(Connection connection){
        if (connection!=null){
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
