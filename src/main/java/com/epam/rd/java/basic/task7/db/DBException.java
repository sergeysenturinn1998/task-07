package com.epam.rd.java.basic.task7.db;

public class DBException extends Exception {
    private String message;

    public DBException(String message, Throwable cause) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "message='" + message + "'";
    }
}
