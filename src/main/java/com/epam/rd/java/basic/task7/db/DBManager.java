package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;

//    private static final String URL = "jdbc:mysql://localhost:3306/myDb";
//    private static final String user = "root";
//    private static final String password = "12345";

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }


    DBManager() {
        ///
    }

    private static final String SQL_INSERT_USER = "insert into users values(default,?)";
    private static final String SQL_FIND_ALL_USERS = "select * from users";
    private static final String SQL_INSERT_TEAM = "insert into teams values (default,?)";
    private static final String SQL_FIND_ALL_TEAMS = "select * from teams";
    private static final String SQL_SET_USERS_TEAMS = "insert into users_teams values (?,?)";
    private static final String SQL_FIND_USER_BY_LOGIN = "select * from users where login = ?";
    private static final String SQL_FIND_TEAM_BY_NAME = "select * from teams where name = ?";
    private static final String SQL_FIND_USERS_TEAMS_BY_ID = "select * from users_teams where user_id=?";
    private static final String SQL_DELETE_USER = "delete from users where id=?";
    private static final String SQL_DELETE_TEAM = "delete from teams where id=?";
    private static final String SQL_UPDATE_TEAMS = "update teams set name=? where id=?";


    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        try {
            con = DbUtils.getConnection();
            st = con.createStatement();
            rs = st.executeQuery(SQL_FIND_ALL_USERS);
            while (rs.next()) {
                int id = rs.getInt("id");
                String login = rs.getString("login");
                User user = new User();
                user.setLogin(login);
                user.setId(id);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("cannot find all users", e);
        } finally {
            DbUtils.close(rs);
            DbUtils.close(st);
            DbUtils.close(con);
        }
    }

    public boolean insertUser(User user) throws DBException {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            connection = DbUtils.getConnection();
            pstmt = connection.prepareStatement(SQL_INSERT_USER,
                    Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, user.getLogin());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot insert user", e);
        } finally {
            DbUtils.close(rs);
            DbUtils.close(pstmt);
            DbUtils.close(connection);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection con = null;
        PreparedStatement prpstmt = null;
        try {
            con = DbUtils.getConnection();
            con.setAutoCommit(false);
            prpstmt = con.prepareStatement(SQL_DELETE_USER);

            for (User user : users) {
                prpstmt.setInt(1, user.getId());
                prpstmt.addBatch();
            }

            int[] deleteUsers = prpstmt.executeBatch();
            for (int k : deleteUsers) {
                if (k != 1) {
                    return false;
                }
            }
            con.commit();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("cannot delete user", e);
        } finally {
            DbUtils.close(prpstmt);
            DbUtils.close(con);
        }
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        Connection connection = null;
        PreparedStatement prpstmt = null;
        ResultSet rs = null;

        try {
            connection = DbUtils.getConnection();
            prpstmt = connection.prepareStatement(SQL_FIND_USER_BY_LOGIN);
            prpstmt.setString(1, login);
            rs = prpstmt.executeQuery();

            while (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
            }
            return user;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("cannot get user by login", e);
        } finally {
            DbUtils.close(rs);
            DbUtils.close(prpstmt);
            DbUtils.close(connection);
        }
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        Connection connection = null;
        PreparedStatement prpstmt = null;
        ResultSet rs = null;

        try {
            connection = DbUtils.getConnection();
            prpstmt = connection.prepareStatement(SQL_FIND_TEAM_BY_NAME);
            prpstmt.setString(1, name);
            rs = prpstmt.executeQuery();

            while (rs.next()) {
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
            }
            return team;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("cannot get team by name", e);
        } finally {
            DbUtils.close(rs);
            DbUtils.close(prpstmt);
            DbUtils.close(connection);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        try {
            connection = DbUtils.getConnection();
            statement = connection.createStatement();
            rs = statement.executeQuery(SQL_FIND_ALL_TEAMS);

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");

                Team team = new Team();
                team.setId(id);
                team.setName(name);
                teams.add(team);
            }
            return teams;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("cannot find all teams", e);
        } finally {
            DbUtils.close(rs);
            DbUtils.close(statement);
            DbUtils.close(connection);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            connection = DbUtils.getConnection();
            pstmt = connection.prepareStatement(SQL_INSERT_TEAM,
                    Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, team.getName());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
            }
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot insert team", e);
        } finally {
            DbUtils.close(rs);
            DbUtils.close(pstmt);
            DbUtils.close(connection);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement prpstmt = null;

        try {
            con = DbUtils.getConnection();
            con.setAutoCommit(false);

            prpstmt = con.prepareStatement(SQL_SET_USERS_TEAMS);
            for (Team team : teams) {
                prpstmt.setInt(1, user.getId());
                prpstmt.setInt(2, team.getId());
                prpstmt.executeUpdate();
            }
            con.commit();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            DbUtils.rollback(con);
            throw new DBException("cannot set users_teams table", e);
            //
        } finally {
            DbUtils.close(prpstmt);
            DbUtils.close(con);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> usersTeams = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = DbUtils.getConnection();
            stmt = con.prepareStatement(SQL_FIND_USERS_TEAMS_BY_ID);
            stmt.setInt(1, user.getId());
            rs = stmt.executeQuery();

            while (rs.next()) {
                stmt = con.prepareStatement("select * from teams where id = " + rs.getInt("team_id"));
                ResultSet resultSet = stmt.executeQuery();

                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("name");

                    Team team = new Team();
                    team.setName(name);
                    team.setId(id);
                    usersTeams.add(team);
                }
            }
            return usersTeams;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("cannot get users_teams", e);
        } finally {
            DbUtils.close(rs);
            DbUtils.close(stmt);
            DbUtils.close(con);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement prpstmt = null;

        try {
            con = DbUtils.getConnection();
            prpstmt = con.prepareStatement(SQL_DELETE_TEAM);
            prpstmt.setInt(1, team.getId());
            prpstmt.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("cannot delete team", e);
        } finally {
            DbUtils.close(prpstmt);
            DbUtils.close(con);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement prpstmt = null;

        try {
            con = DbUtils.getConnection();
            prpstmt = con.prepareStatement(SQL_UPDATE_TEAMS);

            prpstmt.setString(1, team.getName());
            prpstmt.setInt(2, team.getId());

            prpstmt.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("cannot update teams", e);
        } finally {
            DbUtils.close(prpstmt);
            DbUtils.close(con);
        }
    }
}
